import React from 'react';
import Answer from '../q1_buttons/answer';
import { Meta } from '@storybook/react/types-6-0';

export const Question1: React.FC = () => (
    <Answer />
)

export default {
    title: 'Questions/1__Theme-Button',
    component: Question1,
    argTypes: {
      backgroundColor: { control: 'color' },
    },
  } as Meta;