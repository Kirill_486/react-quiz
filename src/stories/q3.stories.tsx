import React from 'react';
import Answer from '../q3_refactor/answer';
import { Meta } from '@storybook/react/types-6-0';

export const Question3: React.FC = () => (
    <Answer />
)

export default {
    title: 'Questions/3__Refactoring',
    component: Question3,
    argTypes: {
      backgroundColor: { control: 'color' },
    },
  } as Meta;