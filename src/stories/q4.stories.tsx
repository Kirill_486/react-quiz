import React from 'react';
import Answer from '../q4_ant-design/answer';
import { Meta } from '@storybook/react/types-6-0';

export const Question4: React.FC = () => (
    <Answer />
)

export default {
    title: 'Questions/4__Ant',
    component: Question4,
    argTypes: {
      backgroundColor: { control: 'color' },
    },
  } as Meta;
