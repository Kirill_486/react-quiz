import React from 'react';
import Answer from '../q2_cards/answer';
import { Meta } from '@storybook/react/types-6-0';

export const Question2: React.FC = () => (
    <Answer />
)

export default {
    title: 'Questions/2__Cards',
    component: Question2,
    argTypes: {
      backgroundColor: { control: 'color' },
    },
  } as Meta;