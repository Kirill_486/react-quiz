import React, { useState, useEffect } from 'react';

const App: React.FC = () => {
  const initialValue = localStorage.getItem("info") || "";
  const [{value}, setState] = useState({value: initialValue});
  useEffect(() => localStorage.setItem("info", value));
  return (
    <div>
      <input value={value} type="text" onChange={(e) => setState({ value: e.target.value })} />
      <p>{value}</p>
    </div>
  );
}

const answer: React.FC = () => {
    return (
        <App />
    )
}

export default answer;
