import React from 'react';
import { Select } from 'antd';
import { getCountriesInfo } from './answer';
const { Option } = Select;

interface ISelectComponentProps {
    options: string[];
    selected: string;
    setSelected: (selected: string) => void;
}

export const SelectComponent: React.FC<ISelectComponentProps> = ({options, selected, setSelected: setSelkected}) => {
    const countries  = getCountriesInfo();
    const filterOption = (input: string, option: any) => {
        const value = option?.children.toLowerCase();
        return (value.indexOf(input.toLowerCase()) >= 0) ? true : false;
    }
    return (
        <div className="select__container">
            <Select
                showSearch
                placeholder="Select country"
                onChange={setSelkected}
                filterOption={filterOption}
                optionFilterProp="children"
                value={selected}
                className="select__select"
            >
                {options.map((code: string) => {
                    const title = countries[code].titile;
                    return (<Option value={code} key={code}>{title}</Option>)
                })}
            </Select>
        </div>
    )
}