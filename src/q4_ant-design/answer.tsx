import React, { useState } from 'react';
import * as countriesIso from 'i18n-iso-countries';
import {getCountryCallingCode, CountryCode, parsePhoneNumberFromString} from 'libphonenumber-js';
import { SelectComponent } from './select';
import { InputComponent } from './input';
import 'antd/dist/antd.css';
import './index.css';
import { ValidIndicator } from './validIndicator';
import { Button } from 'antd';

// tslint:disable-next-line: no-var-requires
countriesIso.registerLocale(require("i18n-iso-countries/langs/en.json"));
const lang = 'en';

export interface ICountrieInfo {
    titile: string;
    callCode: string;
}

type countriesMap = { [key: string] : ICountrieInfo }

export const getCountriesInfo = () => {
    const codesA2 = Object.keys(countriesIso.getAlpha2Codes());
    const countries: countriesMap = {};

    codesA2.forEach((code) => {

        try {
            const callingCode = getCountryCallingCode(code as CountryCode) as string;
            const info: ICountrieInfo = {
                titile: `${countriesIso.getName(code, lang)} + ${callingCode}`,
                callCode: callingCode,
            }
            countries[code] = info;
        } catch(e) {
            // getCountryCallingCode throws on unsupported country
        }
    });

    return countries;
}

const Answer: React.FC = () => {
    const countries = getCountriesInfo();
    const [selected, setSelected] = useState('CA');
    const prefix = `+ ${countries[selected].callCode}`;
    const [value, setValue] = useState("");

    const phoneNumber = parsePhoneNumberFromString(`${prefix}${value}`);
    const phoneNumberValid = phoneNumber?.isValid();

    const formattedValue = phoneNumber?.formatInternational().substring(prefix.length) || value;

    return (
        <div className="form__container">
            <SelectComponent
                options={Object.keys(countries)}
                selected={selected}
                setSelected={setSelected}
            />
            <div className="form-input">
                <InputComponent
                    number={formattedValue}
                    prefix={prefix}
                    setValue={setValue}
                />
                {phoneNumberValid && <ValidIndicator />}
            </div>
            <Button
                type="primary"
                disabled={!phoneNumberValid}
                onClick={() => alert(phoneNumber?.formatInternational())}
            >
                Submit
            </Button>
        </div>
    )
}

export default Answer;
