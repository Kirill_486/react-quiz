import React from 'react';
import './valid.css';


export const ValidIndicator: React.FC = () => {
    return (
        <div className="valid-dot"></div>
    )
}