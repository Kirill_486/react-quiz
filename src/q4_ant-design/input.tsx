import React, { SyntheticEvent, ChangeEvent } from 'react';
import { Input } from 'antd';
import { command } from '../q2_cards/answer';

interface IInputProps {
    number: string;
    prefix: string;
    setValue: command;
}

export const InputComponent: React.FC<IInputProps> = ({number, prefix, setValue}) => {
    const onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        const value = e.currentTarget.value;
        const numbers = value.replace(/\D/g,'');
        setValue(numbers);
    }

    return (
        <div className="input__container">
            <Input
                value={number}
                prefix={prefix}
                onChange={onInputChange}
                className="input__input"
            />
        </div>
    )
}