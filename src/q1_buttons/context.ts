import React from "react";

export enum Themes {
    Light = 'Light',
    Dark = 'Dark',
    Blue = 'Blue',
}

export const nextTheme = (theme: Themes) => {
    switch (theme) {
        case Themes.Light: return Themes.Dark;
        case Themes.Dark: return Themes.Blue;
        case Themes.Blue: return Themes.Light;
    }
}

const ThemeContext = React.createContext(Themes.Light);
export default ThemeContext;
