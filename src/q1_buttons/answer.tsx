import React, { useContext, useState } from 'react';
import ThemeContext, { Themes, nextTheme } from './context';

import './index.css';
import { command } from '../q2_cards/answer';
import AppTheme, { IApplicationTheme } from './themes';

const Answer: React.FC = () => {
    const [theme, setTheme] = useState(Themes.Light);
    return (
        <ThemeContext.Provider value={theme}>
            <ThemeSwitcher
                buttonLabel={`Switch to ${nextTheme(theme)} Theme`}
                switchTheme={() => setTheme(nextTheme(theme))}
                theme={theme}
            />
        </ThemeContext.Provider>
    )
}

interface IThemeSwitcherProps {
    theme: Themes;
    buttonLabel: string;
    switchTheme: command;
}

const ThemeSwitcher: React.FC<IThemeSwitcherProps> = ({buttonLabel, switchTheme, theme}) => {
    const themeKey = useContext(ThemeContext);
    const {backgroundColor, buttonColor, textColor}: IApplicationTheme = AppTheme[themeKey];
    return (
        <div
            className="switcher-container"
            style = {{
                backgroundColor,
                }
            }
        >
            <span
                className="switcher-current"
                style={{
                    color: buttonColor,
                }}
                >{`Current: ${theme}`}
            </span>
            <button
                className="switcher-button"
                onClick={switchTheme}
                style={{
                    color: textColor,
                    backgroundColor: buttonColor,
                }}
            >{buttonLabel}</button>
        </div>
    );
}

export default Answer;
