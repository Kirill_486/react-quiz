import { Themes } from "./context";

export interface IApplicationTheme {
    textColor: string;
    buttonColor: string;
    backgroundColor: string;
}

export type AppThemeMap = {[key: string]: IApplicationTheme }

const AppTheme: AppThemeMap = {
    [Themes.Light]: {
        textColor: "white",
        buttonColor: "black",
        backgroundColor: "white",
    },
    [Themes.Dark]: {
        textColor: "black",
        buttonColor: "white",
        backgroundColor: "black",
    },
    [Themes.Blue]: {
        textColor: "white",
        buttonColor: "grey",
        backgroundColor: "lightblue"
    }
}

export default AppTheme;