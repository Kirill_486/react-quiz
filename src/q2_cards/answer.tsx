import React from 'react';

const refreshThePage = () => {
    window.location.reload();
};
const turnFirstCardBlue = () => {
    const firstCard = document.querySelectorAll('.card')[0] as HTMLDivElement;
    firstCard.style.backgroundColor = 'blue';
}

export const Cards: ICardProps[] = [{
    imgUrl: 'https://picsum.photos/300/300',
    text: 'This is the text of first card',
    buttonLabel: 'Click me to refresh the page',
    cardAction: refreshThePage,
},
{
    imgUrl: 'https://picsum.photos/300/300',
    text: 'This is the text of the card that follows',
    buttonLabel: 'Click me to turn first card background blue',
    cardAction: turnFirstCardBlue,
}];

export type command = (...args: any[]) => void;

interface ICardStateProps {
    imgUrl: string;
    text: string;
    buttonLabel: string;
}

interface ICardDispatchProps {
    cardAction: command;
}


interface ICardProps extends
ICardStateProps,
ICardDispatchProps
{}

export const Card: React.FC<ICardProps> = ({text, imgUrl, buttonLabel, cardAction}) => (
    <div className="card">
        <div className="card-header">
              <img src={imgUrl} />
        </div>
    <div className="card-content">
        <p>{text}</p>
        <button onClick={cardAction}>{buttonLabel}</button>
    </div>
</div>
);

const answerStory: React.FC = () => {
    return (
        <div className="section">
            <h1>Card Component</h1>
            {Cards.map(({imgUrl, text, buttonLabel, cardAction}, index) => (
                <Card
                    text={text}
                    imgUrl={imgUrl}
                    buttonLabel={buttonLabel}
                    cardAction={cardAction}
                    key={index}
                />
            ) )}
        </div>
    )
}

export default answerStory;
