This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Installation & Run

**Firstly:** Type these commands in your terminal


```
git clone https://Kirill_486@bitbucket.org/Kirill_486/react-quiz.git

cd react-quiz

npm install

npm run storybook
```

**Then:** You will see storybook tests envioroment.
![TestEnvioroment](/img/storybook.png)

Each section provides the answer to the corresponding Quiz Question.

## Question 1 Buttons

Create a button component on a simple white background. Using React Hooks and Context create a theme switcher button, to cycle through 3 themes. In Light theme, the button should be black and the background should be white, in the Dark theme the button should be white and the background should be black, and in Blue theme, the background should be light blue and the button should be grey. Button label should indicate the next theme, ex. "Switch to Light theme".

## Question 2 Cards

Create the following HTML elements as a Section Component, 2 Card Components with different text and pictures, and Button Component in React using Typescript. Note: Please create 3 separate components to be used in a single page to render the below elements. Add actions to buttons that will work as described on their labels

```
<div class="section">
    <h1>Card Component</h1>

    <div class="card">
          <div class="card-header">
                <img src="https://picsum.photos/300/300" />
          </div>
          <div class="card-content">
                <p>This is the text of first card</p>
                <button>Click me to refresh the page</button>
          </div>
    </div>

    <div class="card">
        <div class="card-header">
              <img src="https://picsum.photos/300/300" />
        </div>
        <div class="card-content">
              <p>This is the text of the card that follows</p>
              <button>Click me to turn first card background blue</button>
        </div>
    </div>

</div>

```

## Question 3 Refactoring

Refactor the code below using react hooks in as little lines. Create any custom hooks when applicable. Best case: 10 lines of code

## Question 4 Input Phone Form

Using ant.design UI library create a Form that will consist of 2 elements: country selector and phone number input. When user selects a country, a mask with country phone number format and country prefix should be added to phone number input. Phone number input should be disabled if country is not selected. All fields are required and should be validated properly. You can use any library for countries or phone format, but we would recommend https://www.npmjs.com/package/i18n-iso-countries and https://www.npmjs.com/package/libphonenumber-js
